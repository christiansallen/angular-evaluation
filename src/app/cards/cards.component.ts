import { Component, OnInit, Input } from "@angular/core";
import { HttpService } from "./../http.service";

@Component({
  selector: "app-cards",
  templateUrl: "./cards.component.html",
  styleUrls: ["./cards.component.scss"]
})
export class CardsComponent implements OnInit {
  @Input() personId: number;
  @Input() firstName: string;
  cards: any = [];
  card = {};
  newCardNumber;
  constructor(private _http: HttpService) {}

  ngOnInit() {
    this.getAllCards(this.personId);
  }

  getAllCards = personId => {
    this._http.getAllCards(personId).subscribe(cards => {
      console.log(cards);
      this.cards = cards;
    });
  };

  addCard = (cardNumber: string, personId: string) => {
    this._http.addCard(cardNumber, personId);
    this.getAllCards(personId);
  };
}
