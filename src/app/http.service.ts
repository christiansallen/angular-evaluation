import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class HttpService {
  baseUrl = "http://localhost:3000";
  constructor(private http: HttpClient) {}

  getPersonList = () => {
    return this.http.post(`${this.baseUrl}/person/list`, {});
  };

  getPerson(id: number) {
    return this.http.post(`${this.baseUrl}/person/list/`, {});
  }

  getAllCards = (id: string) => {
    return this.http.post(`${this.baseUrl}/card/query`, { person: id });
  };

  addCard(card_number: string, person_id: string) {
    let card = {
      card_number: card_number,
      person_id: parseInt(person_id)
    };
    this.http.post(`${this.baseUrl}/card/add`, card).subscribe(card => {
      return card;
    });
  }
}
