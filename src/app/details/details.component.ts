import { Component, OnInit } from "@angular/core";
import { HttpService } from "./../http.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"]
})
export class DetailsComponent implements OnInit {
  person: object = {
    id: Number,
    first_name: String,
    last_name: String,
    email: String
  };
  personId: number = this.route.snapshot.params.id;
  people: any = [];

  constructor(private route: ActivatedRoute, private _http: HttpService) {}

  ngOnInit() {
    this._http.getPersonList().subscribe(data => {
      this.people = data;
    });

    this._http.getPerson(this.personId).subscribe(people => {
      for (let i = 0; i < this.people.length; i++) {
        if (people[i].id == this.personId) {
          this.person = people[i];
        }
      }
    });
  }
}
