import { Component, OnInit } from "@angular/core";
import { HttpService } from "./../http.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  homeTitle = "List of People in Database:";

  people: any = [];

  constructor(private _http: HttpService) {}

  ngOnInit() {
    this._http.getPersonList().subscribe(data => {
      this.people = data;
    });
  }

  // clicked = person => {
  //   console.log("person:", person);
  //   const personObject = { person: person.id };
  //   this.router.navigate(["/", person.id]);
  //   this._http.cardQuery(personObject);
  // };
}
